<?php

class Planet {
  
  /**
   * Distance from the system's sun. Ranges from .1 AU to 40 AU. (basing off of solar system and some slight increase)
   * In reality, these seem to follow a distribution pattern, TODO: come up with an algorithm to follow that.
   */
  private $distanceFromStar;
  
  /**
   * Possible classifications (Following major categories in http://en.wikipedia.org/wiki/List_of_planet_types)
   *  -Terrestrial
   *  -Gas
   * May be better to handle these as sub-classes.
   */
  public $classification;
  
  /**
   * Mass in x10^24 kg
   */
  public $mass;
  
  /**
   * radius (thousands of kilometers)
   */
  public $size;
  
  /**
   * Big factor in determining habitability. 
   * TODO: 
   */
  public $averageTemperature;
  
  /**
   * Constructor
   */
  function Planet($forceHabitable) {
      
    if($forceHabitable){
      $this->distanceFromStar = 1.0; 
      $this->averageTemperature = 72;
      $this->classification = 'Terrestrial';
      $this->mass = (mt_rand() / mt_getrandmax()) * 2 + 5;
      $this->radius = 6371 * $this->mass / 5.97219; //proportional to Earth measurements       
      return;
    }
    
    //TODO: make this more distributed.
    $this->distanceFromStar = (mt_rand() / mt_getrandmax()) * 39.9 + 0.1; 
    $this->averageTemperature = 72;
    $this->classification = 'Terrestrial';
    $this->mass = (mt_rand() / mt_getrandmax()) * 2 + 5;
    $this->radius = 6371 * $this->mass / 5.97219; //proportional to Earth measurements    
    
  }
  
}
