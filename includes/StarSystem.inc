<?php


class StarSystem {
  
  private $planets = array();
  
  /**
   * Constructor
   * Must generate the system.
   * 
   * @param boolean $forceHabitable, force a habitable planet to be in this system
   */
  function StarSystem($forceHabitable){
    $this->generatePlanets($forceHabitable);
  }
  
  /**
   * Constructor. Generate the content of this star system.
   */
  private function generatePlanets($forceHabitable) {
    
    //These are quite arbitrary: TODO, look up some science and make this reflect reality. 
    $maxPlanetsPerStar = 14;
    $minPlanetsPerStar = 1;
    $numPlanets = rand($minPlanetsPerStar, $maxPlanetsPerStar);
    
    for($i = 0; $i <= $numPlanets; $i++){
      $this->planets[$i] = new Planet();
    }
    
  }
  
  /**
   * Any point to this?
   */
  private function __destruct() {
    
  }
}
