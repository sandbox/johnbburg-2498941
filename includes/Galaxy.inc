<?php


class Galaxy {
  
  private $systems = array(array());
  
  function Galaxy($numPlanets) {
    
    $max = $numPlanets * $numPlanets;
    
    for($i = 0; $i < $numPlanets; $i++) {
      $placed = FALSE;
      while(!$placed) {
        $x = rand(0, $max);
        $y = rand(0, $max);
        if(!isset($systems[$x][$y])) {
          $placed = TRUE;
        }
      }
    }
    
  }
  
}