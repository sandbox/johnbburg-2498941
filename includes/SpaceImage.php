<?php

/**
 * Generate a star field image.
 */
class SpaceImage {
  
  public $image;
  public $x;
  public $y;
  public $stars = array();
  public $starColor;
  
  function SpaceImage($x, $y, $id, $map = FALSE) {
    
    $this->id = $id;
    $this->x = $x;
    $this->y = $y;
        
    if($map) {
      $this->generateImageFromMap($x, $y, $map);
    }
    else {
      $this->generateImage($x, $y);
      //$this->generateMap();
    }
    
    $this->output();
    
    imagecolordeallocate( $starColor );
    imagecolordeallocate( $background );
    //imagedestroy($this->image);
  }
  
  public function generateImage($x, $y) {
    $this->image = imagecreate($x, $y);
    $background = imagecolorallocate( $this->image, 0, 0, 0 );
    $this->starColor = imagecolorallocate($this->image, 255, 255, 255);
        imagesetthickness($this->image, 2 );
    $this->generateMap();

  }
  
  public function generateImageFromMap($x, $y, $map) {
    $this->image = imagecreate($x, $y);
    imagesetthickness( $this->image, 5 );
  }
  
  public function generateMap() {
    $concentration = ($this->x * $this->y) / 8000;
    
    for($i = 0; $i < $concentration; $i++){
      $this->createStar();
    }
    
  }
  
  public function createStar() {
    $location = $this->generateLocation();
    $size = rand(2, 10);
    $width = $size;
    $height = $size; 
    imagefilledellipse( $this->image , $location['x'] , $location['y'] , $width , $height , $this->starColor );
    
  }
  
  public function generateLocation(){
    $validated = false;
    $count = 0;
    while(!$validated && $count < 40){
      $x = rand(20, $this->x - 20);
      $y = rand(20, $this->y - 20);
      $validated = $this->validateLocation($x, $y);
      $count++;
    }
    
    
    
    $location = array('x' => $x, 'y' => $y, 'size' => $size);
    $this->stars[] = $location;
    return $location; 
  }
  
  /**
   * Enforce a minimum distance between two stars. using 10 for now 
   */
  public function validateLocation($x1, $y1){
    foreach ($this->stars as $star) {
      $a = $x1 - $star['x'];
      $b = $y1 - $star['y'];
      $c2 = pow($a, '2') + pow($b, '2') ;
      if(sqrt($c2) < 50){
        return FALSE;
      }
    } 
    return TRUE;
  }
  
  public function output() {
    header( "Content-type: image/png" );
    imagepng($this->image);//, 'drupal_space_' . $this->id);
  }
  
}

$image = new SpaceImage(900, 600, 123);

?>