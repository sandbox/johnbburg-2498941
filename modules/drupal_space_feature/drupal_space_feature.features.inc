<?php
/**
 * @file
 * drupal_space_feature.features.inc
 */

/**
 * Implements hook_node_info().
 */
function drupal_space_feature_node_info() {
  $items = array(
    'drupal_space_game' => array(
      'name' => t('Drupal Space game'),
      'base' => 'node_content',
      'description' => t('A game instance of Drupal Space'),
      'has_title' => '1',
      'title_label' => t('Galaxy name'),
      'help' => '',
    ),
  );
  return $items;
}
